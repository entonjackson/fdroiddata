Categories:Multimedia,Theming
License:GPLv3
Web Site:
Source Code:https://github.com/msal/muzei-nationalgeographic
Issue Tracker:https://github.com/msal/muzei-nationalgeographic/issues

Name:National Geographic for Muzei
Summary:National Geographic pictures for Muzei
Description:
The National Geographic photo of the day for [[net.nurik.roman.muzei]].
.

Repo Type:git
Repo:https://github.com/msal/muzei-nationalgeographic.git

Build:1.1,3
    commit=1.1
    subdir=muzei-nationalgeographic
    gradle=yes

Build:1.1.3,6
    commit=d131f01e35c71cfc43294ac362986afabae9bc38
    subdir=muzei-nationalgeographic
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.3
Current Version Code:6

