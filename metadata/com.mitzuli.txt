Categories:Reading
License:GPLv2+
Web Site:http://www.mitzuli.com
Source Code:https://github.com/artetxem/mitzuli
Issue Tracker:https://github.com/artetxem/mitzuli/issues

Auto Name:Mitzuli
Summary:Offline Translator
Description:
Translator featuring a full offline mode, voice input (ASR), camera input (OCR),
voice output (TTS), and more!

This app periodically updates the list of available languages and their
respective resources in the background. This is necessary for the app to work
robustly, and no personal information is sent to the server.
.

Repo Type:git
Repo:https://github.com/artetxem/mitzuli

Build:1.0.2,10002
    commit=20f2c2f36e593f8c97fadecb3b95ed6cc0bad759
    subdir=app
    gradle=yes
    prebuild=sed -i -e '29,37d' build.gradle && \
        sed -i -e '/splits/,+7d' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

#universal apk
Build:1.0.3,10003
    commit=ee3ef8f6588b721ffca31b2ac17201d184ac9cb7
    subdir=app
    gradle=yes
    prebuild=sed -i -e '29,37d' build.gradle && \
        sed -i -e '/splits/,+7d' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

#universal apk
Build:1.0.4,10004
    commit=a5e701faa4e7c1e6b39847796b8cbdc0bd8820f2
    subdir=app
    gradle=yes
    prebuild=sed -i -e '29,37d' build.gradle && \
        sed -i -e '/splits/,+7d' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

#universal apk
Build:1.0.5,10005
    commit=c7fe1efba47596ef34c2bcfae1662fc50ae7e4a5
    subdir=app
    gradle=yes
    prebuild=sed -i -e '29,37d' build.gradle && \
        sed -i -e '/splits/,+7d' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

Build:1.0.3,1010003
    disable=split
    commit=ee3ef8f6588b721ffca31b2ac17201d184ac9cb7
    subdir=app
    gradle=armeabi
    prebuild=sed -i -e 's/universalApk true/universalApk false/g' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

Build:1.0.3,2010003
    disable=split
    commit=ee3ef8f6588b721ffca31b2ac17201d184ac9cb7
    subdir=app
    gradle=armeabi-v7a
    prebuild=sed -i -e 's/universalApk true/universalApk false/g' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

Build:1.0.3,3010003
    disable=split
    commit=ee3ef8f6588b721ffca31b2ac17201d184ac9cb7
    subdir=app
    gradle=mips
    prebuild=sed -i -e 's/universalApk true/universalApk false/g' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

Build:1.0.3,4010003
    disable=split
    commit=ee3ef8f6588b721ffca31b2ac17201d184ac9cb7
    subdir=app
    gradle=x86
    prebuild=sed -i -e 's/universalApk true/universalApk false/g' build.gradle && \
        echo 'ndkdir=$$NDK$$' > ../gradle.properties
    scanignore=app/src/main/java/com/mitzuli/core/mt/MtPackage.java

Maintainer Notes:
1.0.3 can be build using ABI splits, however all ABIs are built on each run,
splitting and packaging is done afterwards. They are disabled in favor of a
universal apk for now. Once we turned splits on, there is no going back due
to splits having much higher vercodes.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.5
Current Version Code:10005

