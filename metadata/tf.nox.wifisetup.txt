Categories:Security
License:Apache2
Web Site:
Source Code:https://github.com/eqvinox/wifisetup
Issue Tracker:

Auto Name:CCCamp 2015 Wifi Setup
Summary:Create secure Wifi connection entry for CCCamp2015
Description:
Creates a Wifi connection entry for the
[https://events.ccc.de/camp/2015/wiki/Main_Page Chaos Communication Camp 2015],
with proper certificate name and CA checks.
.

Repo Type:git
Repo:https://github.com/eqvinox/wifisetup

Build:0.18,20150810
    commit=f766dfb30373a022cd131f2e189bf4d21b55a49e
    subdir=android

Auto Update Mode:None
Update Check Mode:RepoManifest/camp2015
Current Version:0.18
Current Version Code:20150810

