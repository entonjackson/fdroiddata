Categories:Phone & SMS
License:GPLv3
Web Site:http://tinfoilhat.github.io/tinfoil-sms
Source Code:https://github.com/tinfoilhat/tinfoil-sms
Issue Tracker:https://github.com/tinfoilhat/tinfoil-sms/issues
Donate:https://tinfoilhat.github.io/tinfoil-sms/#donate

Auto Name:Tinfoil-SMS
Summary:Encrypt text messages
Description:
Tinfoil-SMS encrypts your texts. It uses 256 bit ECC public keys as well as a
unique signed key exchange to prevent any "man-in-the-middle" attacks.

WARNING! THIS BUILD USES BINARY FILES FROM UPSTREAM!
.

Repo Type:git
Repo:https://github.com/tinfoilhat/tinfoil-sms.git

Build:1.3.1,16
    disable=orwell and bouncycastle/strippedcastle fail to build
    commit=1.3.1-fdroid
    srclibs=1:NineOldAndroids@2.4.0,Orwell@v1.1,BouncyCastle@r1rv50
    rm=libs/*
    extlibs=android/android-support-v4.jar
    prebuild=cp $$Orwell$$/libs/bcprov-jdk15on-150.jar $$Orwell$$/../bin/orwell-1.1.jar libs/
    scanignore=resources

#Build:1.4.0,20
#    commit=cfe99a6264af818521d1ed6c3f9c18b4089b5f8b
#    srclibs=1:NineOldAndroids@2.4.0,2:Orwell@v1.1,StrippedCastle@5c6236045cc8391d4223d462c56b1d9364a3beaf,JUnit@r4.12,JavaMail@JAVAMAIL-1_5_2
#    rm=libs/*
#    extlibs=android/android-support-v4.jar
#    prebuild=\
#        pushd $$JUnit$$ && $$MVN3$$ package && popd && \
#        pushd $$JavaMail$$ && $$MVN3$$ install -DskipTests && popd && \
#        cp $$JUnit$$/target/junit-4.12.jar $$JavaMail$$/mail/target/javax.mail.jar $$StrippedCastle$$/ && \
#        pushd $$StrippedCastle$$ && \
#        ./become-stripped.sh && \
#        sed -i -e '/src\/test/d' ant/jdk15+.xml && \
#        ant -f ant/jdk15+.xml -lib ./ build-provider && \
#        ant -f ant/jdk15+.xml -lib ./ build && \
#        ant -f ant/jdk15+.xml -lib ./ zip-src && \
#        popd && rm $$Orwell$$/../bin/*jar && rm $$Orwell$$/libs/*jar && cp $$StrippedCastle$$/build/artifacts/jdk1.5/jars/*.jar $$Orwell$$/libs/
Build:1.4.0,20
    disable=builds, but uses jars
    commit=cfe99a6264af818521d1ed6c3f9c18b4089b5f8b
    srclibs=1:NineOldAndroids@2.4.0,Orwell@v1.1
    rm=libs/*
    extlibs=android/android-support-v4.jar
    prebuild=cp $$Orwell$$/../bin/orwell*jar $$Orwell$$/libs/bcprov*jar libs/

Maintainer Notes:
* Use commits from master-fdroid ...
* ... or v.v.v-fdroid tags.
* orwell and bcprov build but
* Patch to switch to gradle is included, wait for Orwell's issue #2.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.4.0
Current Version Code:20

