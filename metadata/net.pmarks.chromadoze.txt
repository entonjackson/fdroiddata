Categories:Multimedia
License:GPLv3
Web Site:https://github.com/pmarks-net/chromadoze/blob/HEAD/README.md
Source Code:https://github.com/pmarks-net/chromadoze
Issue Tracker:https://github.com/pmarks-net/chromadoze/issues

Auto Name:Chroma Doze
Summary:Noise generator
Description:
Generates noise with a custom colour profile. It is intended to be used as a
sleep sound generator. It provides rapid feedback to adjustments in the
spectrum, and is designed to minimize CPU usage in the steady state.

It works by running shaped white noise through an Inverse Discrete Cosine
Transform, generating a few megabytes of distinct audio blocks. The steady-state
behavior selects blocks at random, and smoothly crossfades between them
.

#Repo Type:git-svn
#Repo:https://chromadoze.googlecode.com/svn/trunk
Repo Type:git
Repo:https://github.com/pmarks-net/chromadoze

Build:1.1.1,7
    commit=26

Build:2.1,9
    commit=34
    srclibs=ActionBarSherlock@4.3.0
    extlibs=android/android-support-v4.jar
    prebuild=echo "android.library.reference.1=$$ActionBarSherlock$$" >> project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs
    target=android-16

Build:2.2,10
    commit=37
    srclibs=ActionBarSherlock@4.3.0
    extlibs=android/android-support-v4.jar
    prebuild=echo "android.library.reference.1=$$ActionBarSherlock$$" >> project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs
    target=android-16

Build:3.1,12
    commit=65
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/
    target=android-18

Build:3.3,14
    commit=72
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/
    target=android-18

Build:3.4,15
    commit=76
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/
    target=android-18

Build:3.5,16
    disable=missing resources
    commit=83
    prebuild=cp -R $$SDK$$/extras/android/support/v7/appcompat/ appcompat/ && \
        echo -e "\nandroid.library.reference.1=./appcompat/" >> ./local.properties && \
        $$SDK$$/tools/android update lib-project --path ./appcompat/
    target=android-21

#new repo
Build:3.5.1,17
    commit=v3.5.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:3.5.1
Current Version Code:17

